This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
## Install
### `npm install` `yarn install`
## Available Scripts

In the project directory, you can run:

### `npm start` `yarn start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.


## ToDo

The goal of the task is to create and style the result table using the data provided in the `matches.json` and` teams.json` files.

The `teams.json` file describes teams with the appropriate Id.
The results of matches between these teams are in the `matches.json` file.


The win is `3 points`.
Draw is `1 point` for both teams.
The loss is `0 points`.

Rules:

1. Points.
2. Goal balance in the case of an equal number of points
3. Direct match in case of equal number of points and balance of goals
4. Alphabetical order


We assume that in the basic version the number of points can be equal to a maximum of two teams.


